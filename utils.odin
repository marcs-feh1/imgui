package imgui

import "base:runtime"
import "core:slice"
import "core:mem"

dynamic_array_from_slice :: proc(s: []$T) -> [dynamic]T {
	dyn := runtime.Raw_Dynamic_Array{
		data = raw_data(s),
		len = 0,
		cap = len(s),
		allocator = mem.nil_allocator(),
	}
	return transmute([dynamic]T)dyn
}

// Grows a slice to be beyond its original bounds, be *very* careful when using this.
grow_slice :: proc(s: []$T, amount: int) -> []T {
	raw := runtime.Raw_Slice {
		data = raw_data(s),
		len = len(s) + amount,
	}
	return transmute([]T)raw
}

vector_cast :: proc"contextless"($T: typeid, v: [$N]$E) -> [N]T {
	out: [N]T
	for x, i in v {
		out[i] = cast(T)x
	}
	return out
}

insertion_sort :: proc(s: []$T, lt: proc(T, T) -> bool){
	for i := 1; i < len(s); i += 1 {
		for j := i; j > 0 && !lt(s[j - 1], s[j]); j -= 1 {
			s[j - 1], s[j] = s[j], s[j - 1]
		}
	}
}

rgb :: proc "contextless" (r, g, b: u8) -> Color {
	return Color {
		r, g, b, 255,
	}
}

bytes_of_array :: proc(a: ^[$N]$T) -> []byte {
	ptr := transmute([^]byte)raw_data(a)
	return ptr[:size_of(T) * N]
}

@private
find :: #force_inline proc "contextless" (s: $A/[]$T, key: T) -> (idx: int, found: bool) {
	return slice.linear_search(e, key)
}
