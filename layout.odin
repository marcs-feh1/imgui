package imgui

import "core:mem"
import st "stack"

// Layout :: struct {
// 	origin: ivec2,
// 	padding: Maybe(int), // If `nil` just use the UIContext padding
// 	first_item: int,
// 	item_count: int,
// 	kind: enum byte {
// 		Column = 0,
// 		Row,
// 	},
// 	max_dimensions: ivec2,
// }

Layout :: struct {
	id: Id,
	kind: LayoutKind,
	bounds: Rect,
	measures: [dynamic]int,
	items: []Control,
}

make_layout :: proc(
	ui: ^UIContext,
	kind: LayoutKind,
	count: int,
	allocator := context.allocator,
) -> (layout: Layout, err: mem.Allocator_Error) {
	layout = Layout2 {
		id = gen_id(last_layout_id(ui), kind),
		kind = kind,
	}

	switch kind {
	case .Column_Dynamic, .Row_Dynamic:
		layout.measures = make([dynamic]int, allocator = allocator) or_return
	case .Column_Static, .Row_Static:
		layout.measures = make([dynamic]int, len = 0, cap = count, allocator = allocator) or_return
	case .Absolute:
	}

	return
}

LayoutKind :: enum i8 {
	Column_Dynamic,
	Row_Dynamic,

	Column_Static,
	Row_Static,

	Absolute,
}

// Get layout's next available position for a control
@private
layout_next_slot :: proc(ui: ^UIContext, layout: Layout) -> (slot: Rect, ok: bool) {
	win, w_ok := st.top(ui.windows)
	assert(w_ok, "No windows.")

	padding := layout.padding.? or_else ui.style.padding

	switch layout.kind {
	case .Absolute:
		return
	case .Column_Dynamic:
		unimplemented()
	case .Row_Dynamic:
		unimplemented()
	case .Column_Static:
		width := layout.bounds.w / cap(layout.measures)
		right  := layout.origin.x + width * len(layout)
		top    := layout.origin.y

		slot.pos = ivec2{right, top}
		when ODIN_DEBUG { old_cap := cap(layout.measures) }
		append(&layout.measures, width)
		when ODIN_DEBUG { assert(cap(layout.measures) == old_cap) }


	case .Row_Static:
		offset := layout.max_dimensions.y if len(layout.items) else 0
		right  := layout.origin.x
		top    := layout.origin.y + offset

		slot.pos = ivec2{right, top}
	}

	return slot, true
}

	// case .Column:
	// 	offset := layout.max_dimensions.x if layout.item_count > 0 else 0
	// 	right  := layout.origin.x + offset
	// 	top    := layout.origin.y
	//
	// 	pos = ivec2{right, top}
	//
	// case .Row:
	// 	offset := layout.max_dimensions.y if layout.item_count > 0 else 0
	// 	right  := layout.origin.x
	// 	top    := layout.origin.y + offset
	//
	// 	pos = ivec2{right, top}

// Push new layout to window
push_layout :: proc(ui: ^UIContext, layout: Layout){
	layout := layout
	win, ok := st.top_pointer(&ui.windows)
	assert(ok, "No windows")
	win.layout_count += 1

	top_layout, _ := st.top(ui.layouts)

	layout.origin.x = ui.style.padding
	layout.origin.y = top_layout.origin.y + top_layout.max_dimensions.y + ui.style.padding

	st.push(&ui.layouts, layout)
}

column_layout :: proc(ui: ^UIContext, padding: Maybe(int) = nil){
	push_layout(ui, Layout{
		padding = padding,
		kind = .Column,
	})
}

row_layout :: proc(ui: ^UIContext, padding: Maybe(int) = nil){
	push_layout(ui, Layout {
		padding = padding,
		kind = .Row,
	})
}

next_available_slot :: proc(ui: ^UIContext) -> (pos: ivec2, max_w, max_h: int) {
	layout, ok := st.top(ui.layouts)
	assert(ok, "No layout active")
	pos, max_w, max_h = layout_next_slot(ui, layout)
	return
}

@private
current_layout :: proc(ui: ^UIContext) -> ^Layout {
	layout, ok := st.top_pointer(&ui.layouts)
	assert(ok, "No active layout")
	return layout
}

// Push control to UI, this follows the current layout and window
push_control :: proc(ui: ^UIContext, ctl: Control){
	layout, _ := st.top_pointer(&ui.layouts)
	padding := layout.padding.? or_else ui.style.padding
	if layout.item_count == 0 {
		layout.first_item = st.len(ui.controls)
	}
	layout.item_count += 1

	switch layout.kind {
	case .Column:
		layout.max_dimensions.x += ctl.bounds.w + padding
		layout.max_dimensions.y = max(layout.max_dimensions.y, ctl.bounds.h)

	case .Row:
		layout.max_dimensions.y += ctl.bounds.h + padding
		layout.max_dimensions.x = max(layout.max_dimensions.x, ctl.bounds.w)
	}

	st.push(&ui.controls, ctl)
}

