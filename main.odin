package imgui

import "core:fmt"
import "core:mem"
import "core:math/rand"
import "core:path/filepath"
import "core:os"

import rl "vendor:raylib"
import st "stack"

DEFAULT_FONT :: #load("assets/inconsolata.ttf", []byte)

color_selector :: proc(ui: ^UIContext, color: Color) -> Color {
	column_layout(ui, padding = 1)
	res := rgb(0, 0, 0)

	label(ui, "R:")
	res.r = auto_cast slider(ui, 0, int(color.r), 255, width = 35)
	label(ui, "G:")
	res.g = auto_cast slider(ui, 0, int(color.g), 255, width = 35)
	label(ui, "B:")
	res.b = auto_cast slider(ui, 0, int(color.b), 255, width = 35)

	spacer(ui, SLIDER_HEIGHT)

	rectangle(ui, Rect{ h = SLIDER_HEIGHT * 1.5, w = SLIDER_HEIGHT * 1.5}, res)
	labelf(ui, "(%d, %d, %d)", res.r, res.g, res.b)

	return res
}

main :: proc(){
	when ENABLE_PROFILER {
		profiler_start()
	}

	rl.InitWindow(1000, 650, "demo")
	rl.SetWindowState({.WINDOW_TOPMOST})
	defer rl.CloseWindow()
	rl.SetTargetFPS(60)

	when ODIN_DEBUG {
		tracker : mem.Tracking_Allocator
		mem.tracking_allocator_init(&tracker, context.allocator)
		context.allocator = mem.tracking_allocator(&tracker)
		leak_accumulator := 0
		defer if leak_accumulator > 0 {
			fmt.printfln("Leaked memory: %v KiB", leak_accumulator / 1024)
		}
		defer for ptr, info in tracker.allocation_map {
			fmt.printfln("%v(%v:%v) [%v]: %#p -> %d",
				filepath.base(info.location.file_path),
				info.location.line,
				info.location.column,
				info.location.procedure,
				ptr, info.size)
			leak_accumulator += info.size
		}
	}

	@(static) temp_arena_buf : [32 * mem.Megabyte]byte
	temp_arena : mem.Arena
	mem.arena_init(&temp_arena, temp_arena_buf[:])
	context.temp_allocator = mem.arena_allocator(&temp_arena)

	ui : UIContext
	ui_context_init(&ui)
	defer ui_context_destroy(&ui)

	ui.style.font_size = 15
	ui.style.font = load_font(DEFAULT_FONT, ui.style.font_size)
	defer unload_font(ui.style.font)

	for !rl.WindowShouldClose(){
		defer free_all(context.temp_allocator)
		ui.viewport = get_viewport()

		input: {
			ui.mouse = get_mouse_input(ui.mouse)
			ui.keyboard = get_keyboard_input(ui.keyboard)
		}

		build_ui: {
			ui_begin(&ui); defer ui_end(&ui)

			{
				wid := window(&ui, fmt.tprint("Main window"), {{40, 40}, 500, 300})
				win, _ := get_window(&ui, wid)
				label(&ui, "Window info")
				label(&ui, fmt.tprintf("Position: (%v, %v)", win.body.x, win.body.y))
				label(&ui, fmt.tprintf("Dimensinons: %vx%v", win.body.w, win.body.h))

				@static color : Color
				color = color_selector(&ui, color)

				ui.style.colors[.Window_Background] = color

				@static v := 0;

				column_layout(&ui)
				v = slider(&ui, -300, v, 100)
				label(&ui, fmt.tprint("Value: ", v))

				column_layout(&ui)
				if .NoHandle in win.flags {
					if button(&ui, "Enable Handle"){ win.flags -= {.NoHandle} }
				}
				else {
					if button(&ui, "Disable Handle"){ win.flags += {.NoHandle} }
				}

				column_layout(&ui)
				border_control: {
					if button(&ui, "+"){ ui.style.control_border += 1 }
					labelf(&ui, "Border: %v", ui.style.control_border)
					if button(&ui, "-"){ ui.style.control_border -= 1 }
				}

				padding_control: {
					labelf(&ui, "Padding: %v", ui.style.padding)
					ui.style.padding = slider(&ui, 0, ui.style.padding, 10, scroll_amount=1)
				}
				column_layout(&ui)

				if ui.style.window_border > 0 {
					if button(&ui, "Disable border"){
						ui.style.window_border = 0
						ui.style.control_border = 0
					}
				}
				else {
					if button(&ui, "Enable border"){
						ui.style.window_border = 2
						ui.style.control_border = 1
					}
				}
			}

			if rl.IsKeyPressed(.R){ reset_all_windows(&ui) }
		}

		render: {
			rl.BeginDrawing()
			defer rl.EndDrawing()
			rl.ClearBackground({50, 50, 110, 255})

			ui_render(&ui, auto_clear = false)


			when ODIN_DEBUG {
				@static debug_view := true
				if rl.IsKeyPressed(.W){ debug_view = !debug_view }
				if debug_view {
					for layout in ui.layouts.items {
						rl.DrawRectangleLines(
							i32(layout.origin.x + ui.windows.items[0].body.pos.x),
							i32(layout.origin.y + ui.windows.items[0].body.pos.y),
							i32(layout.max_dimensions.x),
							i32(layout.max_dimensions.y), rl.RED)
					}
					for control in ui.controls.items {
						rl.DrawRectangleLines(
							i32(control.bounds.x),
							i32(control.bounds.y),
							i32(control.bounds.w),
							i32(control.bounds.h), rl.GREEN)
					}
					msg := fmt.tprintf("FPS: %v | Screen %v x %v | Mouse: (%v, %v)\n",
							 rl.GetFPS(),
							 rl.GetScreenWidth(), rl.GetScreenHeight(),
							 ui.mouse.pos.x, ui.mouse.pos.y)
					r_draw_text(ui.style.font, msg, {8, 8}, 16, rgb(80, 220, 80))
				}
			}

			ui.viewport = get_viewport()
			ui_clear(&ui)
		}
	}
}

random_rect :: proc() -> Rect {
	w := rand.int31_max(150) + 200
	h := rand.int31_max(150) + 200
	x := rand.int31_max(400)
	y := rand.int31_max(400)
	return Rect {
		pos = {int(x), int(y)},
		w=int(w), h=int(x),
	}
}
