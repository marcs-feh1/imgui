
## Vocabulary:

- Control: A UI control is something like a button, textbox, scrollbar and so
  on, they are the basic blocks that the user can see and interact.

- Window: Window are "root" containers, that is, they cannot be nested and are
  the minimum required to start rendering controls.

- Layout: Layouts are similar to controls in that they are "owned" by a window.
  The difference being that layouts are invisible and contain metadata to
  organize controls.

- ID: Number to identify a control/window, it is a deterministic hash from a
  particular characteristic of the control (usually its label) + the last used
  hash (to avoid same-name collisions).

- Retained info: To make the end-user's life easier, *some* state is retained
  for windows and certain controls, this is tracked by their Id.

The UI Context is a series of stacks that serve as storage for all the controls

```
Context {
	Windows: [ ... ]
	Controls: [ ... ]
	Layouts: [ ... ]

	RetainedInfo: [ ... ]
}

Window {
	Id
	ZIndex
	Position
	Width, Height
	FirstLayout // Index into layouts stack
	LayoutCount
}

// By default it is a column layout
Layout {
	Position
	FirstItem // Index into controls stack
	ItemCount
	
	// Layout specific info
	MaxHeight
	MaxWidth
	Widths: [ ... ] // Width for each item
}
```

## Rendering the UI

Once the UI Context is set up, the retined state for each window is restored.
Windows are sorted using their Z-index, then, for each window, render its
borders accordingly, then render the window's body by iterating through its
layouts

Whenever a control function is used, such as `button`, it requests the active
layout for a slot to draw, it then pushes the corresponding structure and the
layout children and other metadata gets updated.

For each layout, calculate its controls positions into the screen and render
them accordingly.

