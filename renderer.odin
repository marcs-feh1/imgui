package imgui

import "core:fmt"
import str "core:strings"
import rl "vendor:raylib"
import st "stack"

SLIDER_THICKNESS :: 8

get_viewport :: proc() -> ivec2 {
	w := rl.GetScreenWidth()
	h := rl.GetScreenHeight()
	return {auto_cast w, auto_cast h}
}

measure_text :: proc(text: string, font: Font, size: int) -> (w: int, h: int) {
	cs := str.clone_to_cstring(text, allocator = context.temp_allocator)
	v := rl.MeasureTextEx((transmute(^rl.Font)font)^, cs, f32(size), 0)
	return int(v.x), int(v.y)
}

load_font :: proc(data: []byte, size: int) -> Font {
	font := new(rl.Font)
	font^ = rl.LoadFontFromMemory(".TTF", raw_data(data), auto_cast len(data), auto_cast size, nil, 0)
	return transmute(Font)font
}

unload_font :: proc(font: Font){
	rl_font := transmute(^rl.Font)font
	rl.UnloadFont(rl_font^)
	free(rl_font)
}

ui_render_impl :: proc(ui: ^UIContext){
	colors := &ui.style.colors
	for win in ui.windows.items {
		win_border := ui.style.window_border
		outer_box := rect_expand(win.body, win_border)
		handle := Rect{
			pos = {win.body.pos.x - win_border, win.body.pos.y - win_border - WINDOW_HANDLE_HEIGHT},
			w = win.body.w + win_border * 2,
			h = WINDOW_HANDLE_HEIGHT,
		}

		// Clip everything to be inside current window from now on.
		r_set_clip(rect_union(outer_box, handle))
		defer r_reset_clip()

		if .NoHandle not_in win.flags {
			r_draw_rect(handle, colors[.Window_Handle])
			r_draw_text(ui.style.font, win.label, handle.pos + ui.style.padding, ui.style.font_size, colors[.Window_Foreground])
		}
		draw_box(win.body, win_border, colors[.Window_Background], colors[.Window_Handle])

		layouts := ui.layouts.items[win.first_layout:win.first_layout + win.layout_count]
		for &layout in layouts {
			controls := ui.controls.items[layout.first_item:layout.first_item + layout.item_count]
			for &control in controls {
				switch component in control.component {
				case Spacer:
					// Nothing to do
				case ColorRect:
					r_draw_rect(control.bounds, component.color)
				case Button:
					rect := control.bounds

					color : Color
					switch component.state {
					case .Inactive: color = colors[.Base_Inactive]
					case .Hover: color = colors[.Base_Hover]
					case .Active: color = colors[.Base_Active]
					}
					draw_box(rect, ui.style.control_border, color, colors[.Border])

					tw, th := measure_text(component.label, ui.style.font, ui.style.font_size)
					text_position := ivec2 {
						control.bounds.x + (control.bounds.w - tw) / 2,
						control.bounds.y + (control.bounds.h - th) / 2,
					}

					r_draw_text(ui.style.font, component.label, text_position, ui.style.font_size, colors[.Base_Foreground])

				case Label:
					tw, th := measure_text(component.label, ui.style.font, ui.style.font_size)
					text_position := ivec2 {
						control.bounds.x + (control.bounds.w - tw) / 2,
						control.bounds.y + (control.bounds.h - th) / 2,
					}
					r_draw_text(ui.style.font, component.label, text_position, ui.style.font_size, component.color)

				case Slider:
					rect := control.bounds
					draw_box(rect, ui.style.control_border, colors[.Base_Background], colors[.Border])

					range := component.hi - component.lo
					dist := component.current - component.lo
					bar := (10_000 * dist) / range
					offset := (rect.w * bar) / 10_000

					color : Color
					switch component.state {
					case .Inactive: color = colors[.Base_Inactive]
					case .Hover, .Active: color = colors[.Base_Active]
					}

					pos_x := clamp(rect.x, rect.x + offset, rect.x + rect.w - SLIDER_THICKNESS)

					draw_box(Rect{
						pos = {pos_x, rect.y},
						w = SLIDER_THICKNESS,
						h = rect.h,
					}, ui.style.control_border, color, colors[.Border])
				}
			}
		}
	}
}

@private
draw_box :: proc(r: Rect, border: int, color, border_color: Color){
	r_draw_rect(rect_expand(r, border), border_color)
	r_draw_rect(r, color)
}

r_set_clip :: proc(rect: Rect){
	rl.BeginScissorMode(i32(rect.pos.x), i32(rect.pos.y), i32(rect.w), i32(rect.h))
}

r_reset_clip :: proc(){
	rl.EndScissorMode()
}

r_draw_rect :: proc(rect: Rect, color: Color){
	rl.DrawRectangle(i32(rect.x), i32(rect.y), i32(rect.w), i32(rect.h), auto_cast color)
}

r_draw_text :: proc(font: Font, msg: string, pos: ivec2, size: int, color: Color){
	font := (transmute(^rl.Font)font)^
	msg := str.clone_to_cstring(msg, context.temp_allocator)
	rl.DrawTextEx(font, msg, vector_cast(f32, pos), auto_cast size, 0, auto_cast color)
}

is_mod_key_down :: proc(key: ModKey) -> bool {
	switch key {
	case .Alt: return rl.IsKeyDown(.LEFT_ALT)
	case .Control: return rl.IsKeyDown(.LEFT_CONTROL)
	case .Shift: return rl.IsKeyDown(.LEFT_SHIFT)
	case .Super: return rl.IsKeyDown(.LEFT_SUPER)
	}
	return false
}

get_keyboard_input :: proc(prev: KeyboardInput) -> KeyboardInput {
	input := KeyboardInput{}
	#unroll for k in ModKey {
		if is_mod_key_down(k){
			if .Up in prev.mod_keys[k] {
				input.mod_keys[k] |= {.Pressed}
			}
			input.mod_keys[k] |= {.Down}
		}
		else {
			if .Down in prev.mod_keys[k] {
				input.mod_keys[k] |= {.Released}
			}
			input.mod_keys[k] |= {.Up}
		}
	}

	input.sym = rl.GetCharPressed()
	return input
}

get_mouse_input :: proc(prev: MouseInput) -> MouseInput {
	input := MouseInput{}
	#unroll for b in MouseButton {
		if is_mouse_button_down(b){
			if .Up in prev.buttons[b] {
				input.buttons[b] |= {.Pressed}
			}
			input.buttons[b] |= {.Down}
		}
		else {
			if .Down in prev.buttons[b] {
				input.buttons[b] |= {.Released}
			}
			input.buttons[b] |= {.Up}
		}
	}
	input.pos = vector_cast(int, rl.GetMousePosition())
	scroll_motion := rl.GetMouseWheelMoveV()
	input.scroll = ScrollDirection(scroll_motion.y)
	return input
}

@(private="file")
MOUSE_MAPPING := [MouseButton]rl.MouseButton {
	.Left = .LEFT,
	.Right = .RIGHT,
	.Middle = .MIDDLE,
}

@(private="file")
is_mouse_button_down :: proc(btn: MouseButton) -> bool {
	return rl.IsMouseButtonDown(MOUSE_MAPPING[btn])
}
