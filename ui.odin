package imgui

import "core:hash"
import "core:slice"
import "core:fmt"
import "core:mem"
import la "core:math/linalg"
import sa "core:container/small_array"
import st "stack"

WINDOW_HANDLE_HEIGHT :: 22

Stack :: st.Stack
List :: st.Stack // NOTE: might change this to something similar to Odin's Small_Array

ivec2 :: [2]int
ivec3 :: [3]int
ivec4 :: [4]int

vec2 :: [2]f32
vec3 :: [3]f32
vec4 :: [4]f32

Color :: [4]u8

// Handle to renderer-specific font structure
Font :: distinct rawptr

Control :: struct {
	id: Id,
	bounds: Rect,
	component: union {
		Button,
		Label,
		Slider,
		ColorRect,
		Spacer,
	},
}

ControlFlag :: enum byte {
	Inactive = 0, Hover, Active,
}

// Sliders *always* go from 0 to N, to get negative values simply add an offset.
Slider :: struct {
	lo, hi: int,
	current: int,
	state: ControlFlag,
}

// Only used for fine grained padding
Spacer :: struct {}

ColorRect :: struct {
	color: Color,
}

Button :: struct {
	label: string,
	state: ControlFlag,
}

Label :: struct {
	label: string,
	color: Color,
}

@private MIN_WIN_SIZE :: 16
@private MAX_WIN_SIZE :: 0x800000

ignore_control_input :: #force_inline proc(ui: ^UIContext) -> bool {
	return .Ignore_Control_Input in ui._internal_flags
}

ui_context_init :: proc(ui: ^UIContext, allocator := context.allocator) -> (err: mem.Allocator_Error) {
	ui.windows       = st.create_from_allocator(Window) or_return
	ui.controls      = st.create_from_allocator(Control) or_return
	ui.layouts       = st.create_from_allocator(Layout) or_return
	ui.retained_info = st.create_from_allocator(RetainedInfo) or_return

	ui.style = default_style()

	return
}

ui_context_destroy :: proc(ui: ^UIContext) {
	st.destroy(&ui.windows)
	st.destroy(&ui.controls)
	st.destroy(&ui.layouts)
	st.destroy(&ui.retained_info)
	return
}

UIContext :: struct {
	// Input and Environment
	mouse: MouseInput,
	keyboard: KeyboardInput,
	last_mouse: MouseInput,
	last_keyboard: KeyboardInput,
	viewport: ivec2,

	// Window control data
	hovering: Id,
	resizing: Id,
	grabbing: Id,

	// Focused control
	active_control: Id,

	// Style
	style: Style,

	// UI Immediate state
	windows: Stack(Window),
	controls: Stack(Control),
	layouts: Stack(Layout),

	// UI Retained state
	retained_info: List(RetainedInfo),

	// Private state
	_mode: UIContextMode,
	_internal_flags: UIInternalFlags,
}

UIInternalFlags :: bit_set[enum{
	Window_Being_Created, Ignore_Control_Input,
}; u8]

Window :: struct {
	id: Id,
	body: Rect,
	z_index: int,
	open: bool,
	label: string,
	flags: WindowFlags,

	first_layout: int,
	layout_count: int,
}

WindowFlags :: bit_set[WindowFlag; u8]

WindowFlag :: enum u8 {
	NoHandle,
	Closed,
}

// While this UI library is meant to be "Immediate mode", *some* state is
// retained for convenience and consistency. So the user does not have to track
// a bunch of trivial state changes. The state is identified by the component's Id.
RetainedInfo :: struct {
	id: Id,
	body: Rect,
	scroll: ivec2,
	z_index: int,
	flags: WindowFlags,
}

// Identifier to find components and restore retained state
Id :: distinct i32

Rect :: struct {
	using pos: ivec2,
	w, h: int,
}

mouse_delta :: proc(m0, m1: MouseInput) -> ivec2 {
	return m0.pos - m1.pos
}

// Reset a UI Context
ui_clear :: proc(ui: ^UIContext){
	st.clear(&ui.controls)
	st.clear(&ui.layouts)
	st.clear(&ui.windows)
	ui._internal_flags = {}
	ui._mode = .Clean
}

// Begin building a UI context
ui_begin :: proc(ui: ^UIContext){
	assert(ui._mode == .Clean)
	// TODO: Actually check for mouse interaction too? This is a hack to not
	// trigger buttons when grabbing a window with Ctrl+Click.
	if .Down in ui.keyboard.mod_keys[.Control] {
		ui._internal_flags |= {.Ignore_Control_Input}
	}
	ui._mode = .Building
}

// Render the UI and set it to a Clean state
ui_render :: proc(ui: ^UIContext, auto_clear := true){
	assert(ui._mode == .Finished, "UI context cannot be rendered before being finished. Maybe you forgot 'ui_end()`?")
	ui_render_impl(ui)
	if auto_clear {
		ui_clear(ui)
	}
}

// NOTE: Only run this if windows are already sorted
@private
normalize_z_indices :: proc(ui: ^UIContext){
	for &win, i in ui.windows.items {
		win.z_index = i
	}
}

// Take the new updated retained information from the components and save it
commit_retained_info :: proc(ui: ^UIContext){
	for win in ui.windows.items {
		info, _ := get_retained_info(ui, win.id)
		info.body = win.body
		info.z_index = win.z_index
		info.flags = win.flags
		// info.scroll = win.scroll
	}
}

// Finish UI context, marking it as good to be rendered
ui_end :: proc(ui: ^UIContext){
	assert(ui._mode == .Building, "Cannot end ui context that is not being built")
	sort_windows(ui)
	normalize_z_indices(ui)

	handle_window_events(ui)
	commit_retained_info(ui)

	ui.last_mouse = ui.mouse
	ui._mode = .Finished
}

// Get retained info, if id is not retained, create new entry
get_retained_info :: proc(ui: ^UIContext, id: Id) -> (inf: ^RetainedInfo, created: bool) {
	for &info in ui.retained_info.items {
		if info.id == id {
			return &info, false
		}
	}
	err := st.push(&ui.retained_info, RetainedInfo { id = id })
	assert(err == nil, "Failed to push new info enty")
	top, _ := st.top_pointer(&ui.retained_info)
	return top, true
}

bring_to_top :: proc(ui: ^UIContext, id: Id){
	if id == 0 { return }
	for &win in ui.windows.items {
		if win.id == id {
			win.z_index = 0
		}
		else {
			win.z_index += 1
		}
	}
}

reset_all_windows :: proc(ui: ^UIContext){
	for &win in ui.windows.items {
		win.body.pos = {0, WINDOW_HANDLE_HEIGHT}
		win.body.h = clamp(MIN_WIN_SIZE, win.body.h, MAX_WIN_SIZE)
		win.body.w = clamp(MIN_WIN_SIZE, win.body.w, MAX_WIN_SIZE)
	}
}

window_begin :: proc(ui: ^UIContext, title: string, initial_body: Rect, flags: WindowFlags) -> Id {
	assert(.Window_Being_Created not_in ui._internal_flags, "Windows cannot be nested.")

	win := Window {}
	win.id = gen_id(last_win_id(ui), title)
	win.label = title
	win.flags = flags

	// Restore data
	info, created := get_retained_info(ui, win.id)
	assert(info != nil)

	if created {
		info.body = initial_body
		bring_to_top(ui, win.id)
	}

	win.body    = info.body
	win.flags   = info.flags
	win.z_index = info.z_index
	win.body.w  = clamp(MIN_WIN_SIZE, win.body.w, MAX_WIN_SIZE)
	win.body.h  = clamp(MIN_WIN_SIZE, win.body.h, MAX_WIN_SIZE)

	st.push(&ui.layouts, Layout { origin = ui.style.padding }) // Default layout
	win.first_layout = st.len(ui.layouts) - 1
	win.layout_count += 1

	ui._internal_flags += {.Window_Being_Created}

	st.push(&ui.windows, win)

	return win.id
}

window_handle :: proc(win_body: Rect, pad: int) -> Rect {
	handle := win_body
	handle.h = WINDOW_HANDLE_HEIGHT
	handle.y -= WINDOW_HANDLE_HEIGHT
	handle.w += pad * 2
	handle.x -= pad
	return handle
}

@private
cursor_over_window :: proc(ui: ^UIContext) -> Id {
	for win in ui.windows.items {
		handle := Rect{} if .NoHandle in win.flags else
			window_handle(win.body, ui.style.padding)
		area := rect_union(handle, win.body)
		if point_inside_rect(ui.mouse.pos, area){
			return win.id
		}
	}
	return Id(0)
}

get_window :: proc(ui: ^UIContext, id: Id) -> (win: ^Window, ok: bool) {
	if id == 0 { return }

	for &win, _ in ui.windows.items {
		if win.id == id {
			return &win, true
		}
	}
	return
}

get_control :: proc(ui: ^UIContext, id: Id) -> (control: ^Control, ok: bool){
	if id == 0 { return }

	for &ctrl, _ in ui.controls.items {
		if ctrl.id == id {
			return &ctrl, true
		}
	}
	return
}

// Get all controls that belong to a certain window
@private
window_controls :: proc(ui: ^UIContext, win: Window) -> []Control {
	layouts := ui.layouts.items[win.first_layout:win.first_layout + win.layout_count]
	// fmt.println("Layouts: ", len(layouts))

	first_control := layouts[0].first_item
	control_count := layouts[0].item_count

	for layout in layouts[1:] {
		control_count += layout.item_count
	}

	// fmt.println("Range: ",first_control, first_control + control_count)
	return ui.controls.items[first_control:first_control + control_count]
}

import rl "vendor:raylib"
handle_window_events :: proc(ui: ^UIContext){
	ui.hovering = cursor_over_window(ui)
	// Window is being grabbed, skip everything else
	if ui.grabbing != 0 {
		grabbed, _ := get_window(ui, ui.grabbing)
		delta := mouse_delta(ui.mouse, ui.last_mouse)
		grabbed.body.pos += delta

		// NOTE: I don't know if there is a better solution for this. But the
		// new mouse delta needs to be applied to all the window's children
		// otherwhise there's a latency between the window and the controls,
		// causing a "wobbling" effect
		controls := window_controls(ui, grabbed^)
		for _, i in controls {
			controls[i].bounds.pos += delta
		}

		if .Released in ui.mouse.buttons[.Left] || .Down in ui.mouse.buttons[.Right] {
			ui.grabbing = Id(0)
		}
		return
	}

	// Window is being resized, skip everything else
	if ui.resizing != 0 {
		window, _ := get_window(ui, ui.resizing)
		dims := ui.mouse.pos - window.body.pos
		window.body.w = max(MIN_WIN_SIZE, dims.x)
		window.body.h = max(MIN_WIN_SIZE, dims.y)

		if .Up in ui.mouse.buttons[.Right] || .Down in ui.mouse.buttons[.Left] {
			ui.resizing = 0
		}
	}

	hovered_win, hover_ok := get_window(ui, ui.hovering)
	if !hover_ok { return }

	check_window_resizing: {
		if ui.active_control == 0 &&
			.Down in ui.keyboard.mod_keys[.Control] &&
			.Pressed in ui.mouse.buttons[.Right]
		{
			ui.resizing = ui.hovering
			bring_to_top(ui, ui.hovering)
		}
	}

	check_window_grabbing: {
		handle := Rect{} if .NoHandle in hovered_win.flags else
			window_handle(hovered_win.body, ui.style.padding)

		if point_inside_rect(ui.mouse.pos, handle){
			if .Pressed in ui.mouse.buttons[.Left] {
				ui.grabbing = hovered_win.id
				bring_to_top(ui, ui.hovering)
			}
		}
		else if .Down in ui.keyboard.mod_keys[.Control] && .Pressed in ui.mouse.buttons[.Left] {
			ui.grabbing = ui.hovering
			bring_to_top(ui, ui.hovering)
		}
	}
}

sort_windows :: proc(ui: ^UIContext){
	insertion_sort(ui.windows.items[:], proc(a, b: Window) -> bool {
		return a.z_index < b.z_index
	})
}

window_end :: proc(ui: ^UIContext, _: string = {}, _: Rect = {}, _: WindowFlags = {}){
	// NOTE: Signature is to allow for deffered_in proc
	assert(.Window_Being_Created in ui._internal_flags, "Cannot end window that was not initiated")
	ui._internal_flags -= {.Window_Being_Created}
}

@(deferred_in=window_end)
window :: proc(ui: ^UIContext, title: string, initial_body := Rect{{20, 20}, 200, 200}, flags := WindowFlags{}) -> Id {
	return window_begin(ui, title, initial_body, flags)
}

// Used for verification and syncing.
@private
UIContextMode :: enum byte {
	Clean = 0, // Context has been rendered or just started
	Building,  // Context is being built
	Finished,  // Context has been built and awaits rendering
}

rect_valid :: proc(r: Rect) -> bool {
	return r.w > 0 && r.h > 0
}

rect_intersect :: proc(a, b: Rect) -> Rect {
	i := Rect {
		pos = {
			max(a.x, b.x),
			max(a.y, b.y),
		},
	}
	i.w = min(a.x + a.w, b.x + b.w) - i.x
	i.h = min(a.y + a.h, b.y + b.h) - i.y
	return i
}

rect_union :: proc(a, b: Rect) -> Rect {
	u := Rect {
		pos = {
			min(a.x, b.x),
			min(a.y, b.y),
		},
	}

	u.w = max(a.x + a.w, b.x + b.w) - u.x
	u.h = max(a.y + a.h, b.y + b.h) - u.y
	return u
}

point_inside_rect :: proc(p: ivec2, r: Rect) -> bool {
	return (p.x >= r.x && p.x <= (r.x + r.w)) &&
		   (p.y >= r.y && p.y <= (r.y + r.h))
}

// Expand rectangle by `amount` pixels while preserving its center point
rect_expand :: proc(r: Rect, amount: int) -> Rect {
	e := r
	e.h += amount * 2
	e.w += amount * 2
	e.pos -= amount
	return e
}

@private
gen_id :: proc {
	gen_id_bytes,
	gen_id_layout,
	gen_id_str,
}

@private
gen_id_layout :: proc "contextless" (seed: Id, k: LayoutKind) -> Id {
	x := (Id(k) << 8) ~ seed
	data := transmute([4]byte)(x);
	return gen_id_bytes(seed, data[:])
}

@private
gen_id_bytes :: proc "contextless" (seed: Id, data: []byte) -> Id {
	seed := u32(0x811c9dc5) if seed == 0 else u32(seed)
	code := hash.fnv32a(data, seed)
	return Id(code)
}

@private
gen_id_str :: proc "contextless" (seed: Id, data: string) -> Id {
	return gen_id_bytes(seed, transmute([]byte)data)
}

@private
last_win_id :: proc(ui: ^UIContext ,loc := #caller_location) -> Id {
	win, ok := st.top(ui.windows)
	if !ok { return 0 }
	return win.id
}

@private
last_control_id :: proc(ui: ^UIContext) -> Id {
	ctl, _ := st.top(ui.controls)
	return ctl.id
}

@private
last_layout_id :: proc(ui: ^UIContext) -> Id {
	layout, _ := st.top(ui.layouts)
	return layout.id
}
