package stack

import "base:builtin"
import "base:runtime"
import "core:mem"

Stack :: struct($T: typeid) {
	items: [dynamic]T,
}

create_from_allocator :: proc($T: typeid, allocator := context.allocator) -> (stack: Stack(T), err: mem.Allocator_Error) {
	items := make([dynamic]T, allocator = allocator) or_return
	stack = Stack(T){ items = items }
	return
}

create_from_slice :: proc(slice: []$T) -> Stack(T) {
	backing := runtime.Raw_Dynamic_Array{
		data = raw_data(slice),
		len = 0,
		cap = builtin.len(slice),
		allocator = mem.nil_allocator(),
	}
	return Stack(T){
		items = transmute([dynamic]T)backing,
	}
}

clear :: proc(s: ^Stack($T)){
	builtin.clear(&s.items)
}

create :: proc {
	create_from_slice,
	create_from_allocator,
}

destroy :: proc(s: ^Stack($T)){
	delete(s.items)
}

top :: proc(s: Stack($T)) -> (val: T, ok: bool) {
	if builtin.len(s.items) == 0 {
		return T{}, false
	}
	return s.items[builtin.len(s.items) - 1], true
}

bottom :: proc(s: Stack($T)) -> (val: T, ok: bool) {
	if builtin.len(s.items) == 0 {
		return T{}, false
	}
	return s.items[0], true
}

top_pointer :: proc(s: ^Stack($T)) -> (val: ^T, ok: bool) {
	if builtin.len(s.items) == 0 {
		return nil, false
	}
	return &s.items[builtin.len(s.items) - 1], true
}

bottom_pointer :: proc(s: ^Stack($T)) -> (val: ^T, ok: bool) {
	if builtin.len(s.items) == 0 {
		return nil, false
	}
	return &s.items[0], true
}

push :: proc(s: ^Stack($T), val: T) -> (err: mem.Allocator_Error) {
	append(&s.items, val) or_return
	return
}

pop :: proc(s: ^Stack($T)) -> (val: T, ok: bool ){
	if builtin.len(s.items) == 0 {
		return T{}, false
	}
	v := s.items[builtin.len(s.items) - 1]
	builtin.pop(&s.items)
	return v, true
}

len :: proc {
	len_, len_ptr_,
}

@private
len_ :: proc "contextless" (s: Stack($T)) -> int {
	return builtin.len(s.items)
}

@private
len_ptr_ :: proc "contextless" (s: ^Stack($T)) -> int {
	return builtin.len(s.items)
}

