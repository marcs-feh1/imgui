package imgui

import st "stack"
import "core:fmt"
import "core:math/linalg"

SLIDER_HEIGHT :: 12

rectangle :: proc(ui: ^UIContext, rect: Rect, col: Color){
	pos, _, _ := next_available_slot(ui)
	bounds := Rect {
		pos = pos,
		h = rect.h,
		w = rect.w,
	}

	active := active_window(ui)
	bounds.pos += active.body.pos

	push_control(ui, Control {
		bounds = bounds,
		component = ColorRect {
			color = col,
		},
	})
}

spacer :: proc(ui: ^UIContext, size: int){
	layout := current_layout(ui)
	bounds := Rect{
		h = size,
		w = size,
	}
	when ODIN_DEBUG {
		// Spacers don't *need* a position, however, this is left for visual debugging
		bounds.pos, _, _ = next_available_slot(ui)
		bounds.pos += active_window(ui).body.pos
	}

	push_control(ui, Control {
		id = Id(-1),
		bounds = bounds,
		component = Spacer{},
	})
}

slider :: proc(ui: ^UIContext, lo: int, current_value: int, hi: int, width := 90, scroll_amount := 1) -> int {
	pos, _, _ := next_available_slot(ui)
	current := current_value
	padding := current_layout(ui).padding.? or_else ui.style.padding
	bounds := Rect {
		pos = pos,
		w = width + (padding * 2),
		h = SLIDER_HEIGHT + (padding * 2),
	}

	id := gen_id(last_control_id(ui), []byte{u8(hi), u8(lo)})

	sld := Slider {
		lo = lo, hi = hi,
	}

	active := active_window(ui)
	bounds.pos += active.body.pos

	if ui.active_control == id {
		sld.state = .Active

		dist := f64(ui.mouse.pos.x - bounds.pos.x)
		width := f64(bounds.w)
		mouse_value := linalg.lerp(f64(lo), f64(hi), dist / width)

		current = clamp(lo, int(mouse_value), hi)

		if .Up in ui.mouse.buttons[.Left] {
			ui.active_control = 0
		}
	}
	else if ui.hovering == active.id {
		if point_inside_rect(ui.mouse.pos, bounds) && !ignore_control_input(ui) {
			sld.state = .Hover
			if ui.mouse.scroll != .None {
				current += int(ui.mouse.scroll) * scroll_amount
				sld.state = .Active
			}
			else if .Pressed in ui.mouse.buttons[.Left]{
				ui.active_control = id

				x_dist := ui.mouse.pos.x - bounds.pos.x
				cur := linalg.lerp(f64(lo), f64(hi), f64(x_dist) / f64(bounds.w))
				current = int(cur)
				sld.state = .Active
			}
			// TODO: Arrow keys support
		}
	}

	sld.current = clamp(lo, current, hi)
	push_control(ui, Control{
		id = id,
		bounds = bounds,
		component = sld,
	})

	return sld.current
}

button :: proc(ui: ^UIContext, label: string) -> bool {
	tw, th := measure_text(label, ui.style.font, ui.style.font_size)
	pos, _, _ := next_available_slot(ui)
	padding := current_layout(ui).padding.? or_else ui.style.padding

	bounds := Rect {
		pos = pos,
		w = tw + padding * 4,
		h = th + padding * 4,
	}

	btn := Button {
		label = label,
	}

	active := active_window(ui)
	bounds.pos += active.body.pos

	if ui.hovering != active.id {
		btn.state = .Inactive
	}
	else if point_inside_rect(ui.mouse.pos, bounds) && !ignore_control_input(ui) {
		btn.state = .Hover
		if .Pressed in ui.mouse.buttons[.Left]{
			btn.state = .Active
		}
	}

	push_control(ui, Control{
		id = gen_id(last_control_id(ui), label),
		bounds = bounds,
		component = btn,
	})

	return btn.state == .Active
}

// Same as label, but uses context's temp_allocator for allocating a formatted string.
labelf :: proc(ui: ^UIContext, label_fmt: string, args: ..any){
	text := fmt.tprintf(label_fmt, ..args)
	label(ui, text, label_fmt)
}

// Simple text label.
// The override_text parameter is used for when you have a dynamic label to
// keep the Id hash consistent regardless of formatted values.
label :: proc(ui: ^UIContext, text: string, override_text := ""){
	id_text := text if len(override_text) == 0 else override_text
	tw, th := measure_text(text, ui.style.font, ui.style.font_size)
	pos, _, _ := next_available_slot(ui)
	padding := current_layout(ui).padding.? or_else ui.style.padding
	bounds := Rect {
		pos = pos,
		w = tw + padding * 4,
		h = th + padding * 4,
	}

	active := active_window(ui)
	bounds.pos += active.body.pos

	push_control(ui, Control {
		id = gen_id(last_control_id(ui), id_text),
		bounds = bounds,
		component = Label {
			label = text,
			color = rgb(250, 250, 250),
		}
	})

}

@(private="file")
active_window :: proc(ui: ^UIContext) -> Window {
	active, _ := st.top(ui.windows)
	return active
}

