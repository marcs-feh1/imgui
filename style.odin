package imgui

ColorSpecifier :: enum {
	Base_Background,
	Base_Foreground,

	Window_Handle,
	Window_Background,
	Window_Foreground,

	Border,

	Base_Active,
	Base_Hover,
	Base_Inactive,
}

default_style :: proc() -> Style {
	colors := [ColorSpecifier]Color {
		.Base_Background   = rgb(40, 40, 40),
		.Base_Foreground   = rgb(200, 200, 200),
		.Border            = rgb(10, 10, 10),
		.Window_Background = rgb(30, 30, 30),
		.Window_Foreground = rgb(200, 200, 200),
		.Window_Handle     = rgb(10, 10, 10),
		.Base_Inactive     = rgb(60, 60, 60),
		.Base_Hover        = rgb(75, 75, 75),
		.Base_Active       = rgb(150, 150, 150),
	}
	style := Style{
		colors = colors,
		padding = 3,
		window_border = 2,
		control_border = 1,
	}
	return style
}

Style :: struct {
	font: Font,
	font_size: int,

	padding: int,
	window_border: int,
	control_border: int,
	slider_thickness: int,

	colors: [ColorSpecifier]Color,
}

